(define-module (eku packages haskell)
  #:use-module (guix packages)
  #:use-module (guix download)
  ;; #:use-module (guix utils) ; for substitute-keyword-arguments
  ;; #:use-module (guix gexp)
  ;; #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  ;; #:use-module (guix build-system haskell)
  ;; #:use-module (gnu packages haskell-check)
  ;; #:use-module (gnu packages haskell-web)
  ;; #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages finance)
  #:use-module ((guix licenses) #:prefix license:))

(define-public hledger-bin
  (package
   (inherit hledger)
   (version "1.40")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/simonmichael/hledger/releases/download/"
                                version "/hledger-linux-x64.tar.gz"))
            (sha256 (base32 "17k7b1bcynqa5pwrg8jx4qknx7jx2l6rqvwsggbnhvpf86cnfhcg"))))
   (inputs '())
   (build-system copy-build-system)
   (arguments
    '(#:install-plan '(("hledger" "bin/")
                       ("hledger-ui" "bin/"))))))
