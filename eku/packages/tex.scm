(define-module (eku packages tex)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages nss)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages compression)
  #:use-module (guix build-system texlive)
  #:use-module (nonguix build-system binary)
  #:use-module ((guix licenses) #:prefix license:))

(define-public texlive-siunitx
  (package
   (name "texlive-siunitx")
   (version "3.2.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/josephwright/siunitx/")
                  (commit "50bbaf195cbbc9dab4ae98b7fa9c7306e2372c2d")))
            (sha256
             (base32
              "055dmbn2mgz0b3vl4swlnq6kzkk8wbsnpbj9g2kjyvm41wv44jm6"))))
   (build-system texlive-build-system)
   ;; Rename siunitx to siunitx3 so it is not overwritten by the siunitx (v2) included in textlive.
   (arguments
    '(#:tex-directory "latex/siunitx3"
      #:phases
      (modify-phases %standard-phases
                     (add-after 'build 'move-files
                                (lambda _
                                  (rename-file "build/siunitx.sty" "build/siunitx3.sty"))))))
   (propagated-inputs
    (list texlive-l3kernel texlive-l3packages))
   (home-page "http://www.ctan.org/pkg/siunitx")
   (synopsis "Comprehensive SI units package")
   (description
    "Typesetting values with units requires care to ensure that the combined
mathematical meaning of the value plus unit combination is clear.  In
particular, the SI units system lays down a consistent set of units with rules
on how they are to be used.  However, different countries and publishers have
differing conventions on the exact appearance of numbers (and units).  A
number of LaTeX packages have been developed to provide consistent application
of the various rules.  The @code{siunitx} package takes the best from the
existing packages, and adds new features and a consistent interface.  A number
of new ideas have been incorporated, to fill gaps in the existing provision.
The package also provides backward-compatibility with @code{SIunits},
@code{sistyle}, @code{unitsdef} and @code{units}.  The aim is to have one
package to handle all of the possible unit-related needs of LaTeX users.")
   (license license:lppl1.3c)))

(define-public zotero
  (package
   (name "zotero")
   (version "6.0.26")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://download.zotero.org/client/release/"
                                version "/Zotero-" version "_linux-x86_64.tar.bz2"))
            (sha256
             (base32
              "0h1bizcqlk22h7nvhnyp3ppymv2hrk7133hgmp15hl3bvfzz7nh6"))))
   (inputs (list bash nss `(,gcc "lib") glib glibc dbus-glib libxt gtk+ libx11 zlib))
   (build-system binary-build-system)
   ;; based on @oatmeal@emacs.ch's version
   (arguments
    '(#:install-plan '(("." "share/zotero"))
      #:patchelf-plan `(("zotero-bin" ("glibc" "gcc" "zlib" "gtk+"))
                        ("libmozgtk.so" ("glibc" "gcc" "zlib" "gtk+"))
                        ("libxul.so" ("glibc" "gcc" "zlib" "gtk+" "libx11" "dbus-glib" "libxt" "glib")))
      #:phases
      (modify-phases %standard-phases
                     (delete 'patch-dot-desktop-files)
                     (add-after 'install 'make-wrapper-and-desktop
                                (lambda* (#:key inputs outputs #:allow-other-keys)
                                  (let* ((out (assoc-ref outputs "out"))
                                         (bash (search-input-file inputs "bin/bash"))
                                         (dir (string-append out "/share/zotero"))
                                         (wrapper (string-append out "/bin/zotero"))
                                         (desktop (string-append out "/share/applications/zotero.desktop"))
                                         (icon (string-append out "/chrome/icons/default/default256.png")))
                                    (mkdir-p (dirname wrapper))
                                    (mkdir-p (dirname desktop))
                                    (with-output-to-file wrapper
                                      (lambda _
                                        (format #t
                                                (string-append
                                                 "#!~a~%\n"
                                                 "cd ~a~%\n"
                                                 "./zotero-bin -app ./application.ini $@\n")
                                                bash dir)))
                                    (chmod wrapper #o755)
                                    (with-output-to-file desktop
                                      (lambda _
                                        (format #t
                                                (string-append
                                                 "[Desktop Entry]\n"
                                                 "Name=Zotero\n"
                                                 "Exec=bash -c \"~a/zotero -url %U\"\n"
                                                 "Icon=~a\n"
                                                 "Type=Application\n"
                                                 "Terminal=false\n"
                                                 "Categories=Office;\n"
                                                 "MimeType=text/plain;x-scheme-handler/zotero;application/x-research-info-systems;text/x-research-info-systems;text/ris;application/x-endnote-refer;application/x-inst-for-Scientific-info;application/mods+xml;application/rdf+xml;application/x-bibtex;text/x-bibtex;application/marc;application/vnd.citationstyles.style+xml\n"
                                                 "X-GNOME-SingleWindow=true\n")
                                                dir icon)))))))))
   (home-page "https://www.zotero.org/")
   (synopsis "Your personal research assistant")
   (description
    "Zotero is a free, easy-to-use tool to help you collect, organize, annotate, cite, and share research.")
   (license license:agpl3)))

(define-public zotero-beta
  (package
   (name "zotero")
   (version "7.0.0-beta")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://download.zotero.org/client/beta/"
                                version ".64%2B8f8c0d079/Zotero-" version ".64%2B8f8c0d079_linux-x86_64.tar.bz2"))
            (file-name "Zotero-7.0.0-beta_linux-x86_64.tar.bz2") ; forbidden % in file name
            (sha256
             (base32
              "0724pahjhw68m1kihb33v07w5yvlxry1s97zvzpk0n6sc27zlgw3"))))
   (inputs (list bash nss `(,gcc "lib") glib glibc dbus-glib libxt gtk+ libx11 zlib alsa-lib libxtst))
   (build-system binary-build-system)
   (arguments
    '(#:install-plan '(("." "share/zotero"))
      #:patchelf-plan `(("zotero-bin" ("glibc" "gcc" "zlib" "gtk+"))
                        ("libmozgtk.so" ("glibc" "gcc" "zlib" "gtk+"))
                        ("libxul.so" ("glibc" "gcc" "zlib" "gtk+" "libx11" "dbus-glib" "libxt" "glib" "alsa-lib" "libxtst")))
      #:phases
      (modify-phases %standard-phases
                     (delete 'patch-dot-desktop-files)
                     (add-after 'install 'make-wrapper-and-desktop
                                (lambda* (#:key inputs outputs #:allow-other-keys)
                                  (let* ((out (assoc-ref outputs "out"))
                                         (bash (search-input-file inputs "bin/bash"))
                                         (dir (string-append out "/share/zotero"))
                                         (wrapper (string-append out "/bin/zotero"))
                                         (desktop (string-append out "/share/applications/zotero.desktop"))
                                         (icon (string-append out "/share/zotero/icons/icon128.png")))
                                    (mkdir-p (dirname wrapper))
                                    (mkdir-p (dirname desktop))
                                    (with-output-to-file wrapper
                                      (lambda _
                                        (format #t
                                                (string-append
                                                 "#!~a~%\n"
                                                 "cd ~a~%\n"
                                                 "./zotero-bin -app ./app/application.ini $@\n")
                                                bash dir)))
                                    (chmod wrapper #o755)
                                    (with-output-to-file desktop
                                      (lambda _
                                        (format #t
                                                (string-append
                                                 "[Desktop Entry]\n"
                                                 "Name=Zotero\n"
                                                 "Exec=bash -c \"~a/zotero -url %u\"\n"
                                                 "Icon=~a\n"
                                                 "Terminal=false\n"
                                                 "Type=Application\n"
                                                 "Categories=Office;\n"
                                                 "MimeType=x-scheme-handler/zotero\n")
                                                dir icon)))))))))
   (home-page "https://www.zotero.org/")
   (synopsis "Your personal research assistant")
   (description
    "Zotero is a free, easy-to-use tool to help you collect, organize, annotate, cite, and share research.")
   (license license:agpl3)))
