(define-module (eku packages desktop)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils) ; for substitute-keyword-arguments
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system font)
  #:use-module (nonguix build-system binary)
  #:use-module (gnu packages java)
  #:use-module (gnu packages ebook)
  #:use-module (gnu packages imagemagick)
  #:use-module ((guix licenses) #:prefix license:))

(define-public i3-dracula-theme
  (package
   (name "i3-dracula-theme")
   (version "20220313")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/dracula/i3/")
                  (commit "c75c857bcdb826d55ce360e3d5f5a56ef6783c34")))
            (sha256
             (base32
              "0iv0fcv5hls6qbl3a1pfi6gxzawb9k1qi5260v1vjc4h6f1v0fbd"))))
   (build-system copy-build-system)
   (arguments
    '(#:install-plan
      '((".config/i3/config" "share/dracula/i3")
        (".config/i3status/config" "share/dracula/i3status"))))
   (home-page "https://draculatheme.com/")
   (synopsis "Dark theme for i3")
   (description
    "Dark theme for 237+ apps. To use it, append 'include $PROFILE/share/dracula/i3' to your i3 configuration (~/.config/i3/config) and/or append the content of $PROFILE/share/dracula/i3status to your i3status configuration (~/.config/i3status/config).")
   (license license:expat)))

(define-public github-cli
  (package
   (name "gh")
   (version "2.27.0")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/cli/cli/releases/download/v"
                                version "/gh_" version "_linux_amd64.tar.gz"))
            (sha256
             (base32
              "0ivpc85cs35yrx7immcn67pa3ml02ig4rxlj047fkr7d95z9iqm3"))))
   (build-system binary-build-system)
   (home-page "https://cli.github.com/")
   (synopsis "GitHub’s official command line tool")
   (description
    "gh is GitHub on the command line. It brings pull requests, issues, and other GitHub concepts to the terminal next to where you are already working with git and your code.")
   (license license:expat)))

(define-public bis-biswas-voyager-wallpaper
  (package
   (name "bis-biswas-voyager-wallpaper")
   (version "20210531")
   (source (origin
            (method url-fetch)
            (uri "https://cdnb.artstation.com/p/assets/images/images/038/199/423/large/bis-biswas-voyager.jpg")
            (sha256
             (base32
              "0syq951ix09hahki7fjs0vcx00kl6ikvqgfd7ck1miiqdnwnf4cm"))))
   (build-system copy-build-system)
   (arguments
    '(#:install-plan
      '(("bis-biswas-voyager.jpg" "share/wallpapers/bis-biswas-voyager.jpg"))))
   (home-page "https://www.artstation.com/artwork/rArOra")
   (synopsis "Bis Biswas - Voyager")
   (description "")
   (license #f)))

(define-public bis-biswas-voyager-wallpaper-png ; grub only accepts a png
  (package
   (inherit bis-biswas-voyager-wallpaper)
   (name "bis-biswas-voyager-wallpaper-png")
   (native-inputs (list imagemagick))
   (arguments
    '(#:phases
      (modify-phases %standard-phases
                     (add-after 'unpack 'convert-to-png
                                (lambda _
	                          (invoke "convert"
                                          "bis-biswas-voyager.jpg"
                                          "bis-biswas-voyager.png"))))
      #:install-plan
      '(("bis-biswas-voyager.png" "share/wallpapers/bis-biswas-voyager.png"))))))

(define-public cyberre-grub-wallpaper
  (package
   (name "cyberre-grub-wallpaper")
   (version "20220630")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/ChrisTitusTech/Top-5-Bootloader-Themes")
                  (commit "6c04349eed8214f658edbded5b40c755cac5d308")))
            (sha256
             (base32
              "1i8pf60zmqnbm8r74cqdjgnp984isjyskbgiypca8759kapwc2m6"))))
   (build-system copy-build-system)
   (arguments
    '(#:install-plan
      '(("themes/CyberRe/background.png"
         "share/wallpapers/grub_CyberRe.png"))))
   (home-page "https://www.https://github.com/ChrisTitusTech/Top-5-Bootloader-Themes")
   (synopsis "https://github.com/ChrisTitusTech/Top-5-Bootloader-Themes")
   (description "CyberRe background only.")
   (license license:expat)))

;; (define-public darkman
;;   (package
;;    (name "darkman")
;;    (version "v1.5.4")
;;    (source (origin
;;             (method git-fetch)
;;             (uri
;;              (string-append "https://gitlab.com/WhyNotHugo/darkman/-/tags/" version))
;;             ;; (file-name (string-append "adobe-flashplugin-" version ".deb"))
;;             (sha256
;;              (base32
;;               "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"))))
;;    (build-system go-build-system)
;;    (home-page "")
;;    (synopsis "")
;;    (description "")
;;    (license #f)))

;; derived from https://github.com/darkhz/bluetuith/issues/7#issuecomment-1256836267
(define-public bluetuith
  (package
   (name "bluetuith")
   (version "0.1.3")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/darkhz/bluetuith/releases/download/v" version
				"/bluetuith_" version "_Linux_x86_64.tar.gz"))
            (sha256
             (base32
              "0iflblpnn4iiqpwkcc7jg08bvjygdz32p1fs9wg43ccpxn4ihrd5"))))
   (build-system binary-build-system)
   (arguments
    (list
     #:install-plan ''(("bluetuith" "bin/"))
     #:strip-binaries? #true))
   (home-page "https://github.com/darkhz/bluetuith")
   (synopsis "bluetuith")
   (description
    "bluetuith is a TUI-based bluetooth connection manager, which can interact with
bluetooth adapters and devices.  It aims to be a replacement to most bluetooth
managers, like blueman.")
   (license license:expat)))

(define-public font-jetbrains-mono-nerd
  (package
   (name "font-jetbrains-mono-nerd")
   (version "3.0.2")
   (source
    (origin
     (method url-fetch)
     (uri
      (string-append "https://github.com/ryanoasis/nerd-fonts/releases/"
                     "download/v" version "/JetBrainsMono.zip"))
     (sha256
      (base32 "1kpx71i3sqzzzz59c78bfr787h18ljsywvkrp9yr397wid3rg8qz"))))
   (build-system font-build-system)
   (home-page "https://www.jetbrains.com/lp/mono/")
   (synopsis "Patched JetBrains Mono with Nerd icons.")
   (description
    "Nerd Fonts patches developer targeted fonts with a high number of glyphs (icons). Specifically to add a high number of extra glyphs from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons, and others.")
   (license license:asl2.0)))

;; for packages that keep associating themselves to all the files and nothing else works to stop them
(define (make-package-without-mimetype original)
  (package
   (inherit original)
   (name (string-append (package-name original) "-without-mimetypes"))
   (arguments
    (substitute-keyword-arguments
     (package-arguments original)
     ((#:phases phases)
      #~(modify-phases #$phases
                       (delete 'check) ; takes too long, and since the original works…
                       (add-after 'patch-dot-desktop-files 'remove-mimetypes
                                  (lambda* (#:key outputs #:allow-other-keys)
                                    (let* ((out (assoc-ref outputs "out"))
                                           (applications (string-append out "/share/applications"))
                                           (files (find-files applications "\\.desktop$")))
                                      (substitute* files (("^MimeType=(.*)\n$") "")))))))))))

(define-public calibre-without-mimetypes
  (make-package-without-mimetype calibre))

(define-public signal-cli
  (package
   (name "signal-cli")
   (version "0.13.7")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/AsamK/signal-cli/releases/download/v0.13.7/signal-cli-" version ".tar.gz"))
            (sha256 (base32 "1cqkfi2ijfp714k8mb0z4dm4vjadjh7jf5yr565qc51ij6x8mr19"))))
   (inputs (list jbr21))
   (build-system copy-build-system)
   (arguments
    '(#:install-plan '(("lib" "share/java/")
                       ("bin/signal-cli" "share/java/signal-cli.real"))
      #:phases
      (modify-phases %standard-phases
                     (add-after 'install 'make-wrapper
                                (lambda* (#:key inputs outputs #:allow-other-keys)
                                  (let* ((out (assoc-ref outputs "out"))
                                         (dir (string-append out "/share/java"))
                                         (wrapper (string-append out "/bin/signal-cli"))
                                         (libs (find-files (string-append dir "/lib") "\\.jar$"))
                                         (java (search-input-file inputs "bin/java")))
                                    (mkdir-p (dirname wrapper))
                                    (with-output-to-file wrapper
                                      (lambda _
                                        (format #t
                                                (string-append
                                                 "#!/bin/sh\n"
                                                 "CLASSPATH=~a\n"
                                                 "~a -classpath \"$CLASSPATH\" org.asamk.signal.Main \"$@\"\n")
                                                (string-join libs ":") java)))
                                    (chmod wrapper #o755)))))))
   (home-page "https://github.com/AsamK/signal-cli")
   (synopsis "Signal-cli provides an unofficial commandline, JSON-RPC and dbus interface for the Signal messenger.")
   (description "signal-cli is a commandline interface for the Signal messenger. It supports registering, verifying, sending and receiving messages. signal-cli uses a patched libsignal-service-java, extracted from the Signal-Android source code. For registering you need a phone number where you can receive SMS or incoming calls. Signal-cli is primarily intended to be used on servers to notify admins of important events. ")
   (license license:gpl3+)))
