(define-module (eku packages emacs)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module (gnu packages java)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages compression)
  #:use-module (emacs packages melpa)
  #:use-module (emacs build-system melpa)
  #:use-module ((guix licenses) #:prefix license:))

(define-public emacs-next-pgtk-xwidgets-imagemagick
  (package
    (inherit emacs-next-pgtk-xwidgets)
    (name "emacs-next-pgtk-xwidgets-imagemagick")
    (synopsis "Emacs text editor with @code{xwidgets}, @code{pgtk} and @code{imagemagick} support")
    (arguments
     (substitute-keyword-arguments
         (package-arguments emacs-next-pgtk-xwidgets)
       ((#:configure-flags flags #~'())
        #~(cons* "--with-imagemagick" #$flags))))
    (inputs
     (modify-inputs (package-inputs emacs-next-pgtk-xwidgets)
                    (prepend imagemagick)))))

(define-public emacs-pgtk-xwidgets-imagemagick
  (package
   (inherit emacs-pgtk-xwidgets)
   (name "emacs-pgtk-xwidgets-imagemagick")
   (synopsis "Emacs text editor with @code{xwidgets}, @code{pgtk} and @code{imagemagick} support")
   (arguments
    (substitute-keyword-arguments
     (package-arguments emacs-next-pgtk-xwidgets)
     ((#:configure-flags flags #~'())
      #~(cons* "--with-imagemagick" #$flags))))
   (inputs
    (modify-inputs (package-inputs emacs-pgtk-xwidgets)
                   (prepend imagemagick)))))

(define-public emacs-next-pgtk-imagemagick
  (package
   (inherit emacs-next-pgtk)
   (name "emacs-next-pgtk-imagemagick")
   (synopsis "Emacs text editor with @code{pgtk} and @code{imagemagick} support")
   (arguments
    (substitute-keyword-arguments
     (package-arguments emacs-next-pgtk)
     ((#:configure-flags flags #~'())
      #~(cons* "--with-imagemagick" #$flags))))
   (inputs
    (modify-inputs (package-inputs emacs-next-pgtk)
                   (prepend imagemagick)))))

(define-public emacs-pgtk-imagemagick
  (package
   (inherit emacs-pgtk)
   (name "emacs-pgtk-imagemagick")
   (synopsis "Emacs text editor with @code{pgtk} and @code{imagemagick} support")
   (arguments
    (substitute-keyword-arguments
     (package-arguments emacs-pgtk)
     ((#:configure-flags flags #~'())
      #~(cons* "--with-imagemagick" #$flags))))
   (inputs
    (modify-inputs (package-inputs emacs-pgtk)
                   (prepend imagemagick)))))

(define-public emacs-next-pgtk-xwidgets-imagemagick-no-x
  (package
   (inherit emacs-next-pgtk-xwidgets-imagemagick)
   (name "emacs-next-pgtk-xwidgets-imagemagick-no-x")
   (synopsis "Emacs text editor with @code{xwidgets}, @code{pgtk} and @code{imagemagick} support, without @code{X} support")
   (arguments
    (substitute-keyword-arguments
     (package-arguments emacs-next-pgtk-xwidgets-imagemagick)
     ((#:configure-flags flags #~'())
      #~(cons* "--with-x=no" #$flags))))))

(define-public emacs-org-timeline
  (package
   (name "emacs-org-timeline-fork")
   (version "20220522")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/aizensoosuke/org-timeline.git")
           (commit
            "b3ddaf470dc916dce31982534592b6033fe0dc93")))
     (sha256
      (base32
       "0zvl8fiw7q0vn241nnc2rkwx9l8q8k3z70dghybi1627bm2a430p"))))
   (build-system melpa-build-system)
   (propagated-inputs (list emacs-dash))
   (home-page
    "https://github.com/aizensoosuke/org-timeline/")
   (synopsis
    "Add graphical view of agenda to agenda buffer.")
   (description
    "Documentation at https://melpa.org/#/org-timeline.")
   (license #f)))

(define-public emacs-txl
  (package
   (name "emacs-txl")
   (version "20220321")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/tmalsburg/txl.el")
           (commit
            "25f693062439694e205cdda5dd938249179e70f0")))
     (sha256
      (base32
       "1xpk9il1isgccrnmd02v4i982lqb3z2hhcx5g39akc47c09adiks"))))
   (build-system melpa-build-system)
   (propagated-inputs (list emacs-request emacs-guess-language))
   (home-page
    "https://github.com/tmalsburg/txl.el")
   (synopsis
    "High-quality machine translation in Emacs via DeepL’s REST API")
   (description
    "The command txl-translate-region-or-paragraph translates the marked region or, if no region is active, the paragraph to the respective other language. The current language is detected using the guess-language package. The retrieved translation is shown in a separate buffer where it can be reviewed and edited. The original text can be replaced with the (edited) translation via C-c C-c. The translation can be dismissed (without touching the original text) using C-c C-k. If a prefix argument is given (C-u), the text will be translated round-trip to the other language and back.")
   (license #f)))

(define-public emacs-auctex
  (package
   (name "emacs-auctex")
   (version "13.1.9")
   (source
    (origin
     (method url-fetch)
     (uri (string-append "https://elpa.gnu.org/packages/auctex-" version ".tar"))
     (sha256
      (base32 "0dvf7ajfpi68823qv9vav3r1k04gc9bfq2ys3g1rhga2glxn7q9r"))))
   (build-system melpa-build-system)
   (home-page "https://www.gnu.org/software/auctex/")
   (synopsis "Integrated environment for TeX")
   (description
    "AUCTeX is a comprehensive customizable integrated environment for writing input files for TeX, LaTeX, ConTeXt, Texinfo, and docTeX using Emacs or XEmacs.")
   (license license:gpl3+)))

(define-public emacs-texmathp
  (package
   (inherit emacs-auctex)
   (name "emacs-texmathp")
   (arguments '(#:files ("texmathp.el")))
   (description
    "Provides a function to determine if point in a buffer is inside a (La)TeX math environment. This file is extracted from AUCTeX and needed by cdlatex.")))

(define-public ditaa
  (package
   (name "ditaa")
   (version "0.9")
   (source (origin
            (method url-fetch)
            (uri "https://downloads.sourceforge.net/project/ditaa/ditaa/0.9/ditaa0_9.zip")
            (sha256
             (base32
              "12g6k3hacvyw3s9pijli7vfnkspyp37qkr29qgbmq1hbp0ryk2fn"))))
   (build-system copy-build-system)
   (arguments
    '(#:install-plan
      '(("ditaa0_9.jar" "share/emacs/site-lisp/contrib/scripts/ditaa.jar"))))
   (native-inputs (list unzip))
   (propagated-inputs (list icedtea))
   (home-page "https://ditaa.sourceforge.io/")
   (synopsis "DIagrams Through Ascii Art")
   (description "ditaa is a small command-line utility written in Java, that can convert diagrams drawn using ascii art ('drawings' that contain characters that resemble lines like | / - ), into proper bitmap graphics.")
   (license license:gpl2+)))

(define-public emacs-org-multi-clock
  (package
   (name "emacs-org-multi-clock")
   (version "20210426")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://gitlab.com/OlMon/org-multi-clock")
           (commit
            "cda91b282368172df4877e5d763da7fc6fec8447")))
     (sha256
      (base32
       "17dijq37kw1z7gbh8ikzbhs2392xf23z17iq448s9j3x6j8sspwq"))))
   (build-system melpa-build-system)
   (home-page
    "https://gitlab.com/OlMon/org-multi-clock")
   (synopsis
    "Multiple org-clocks in parallel.")
   (description
    "org-multi-clock is a small package that extends the functionality of
  org-mode clocking system. It does not change the default behavior, it adds two functions to have
  multiple org-clocks in parallel.")
   (license #f)))

(define-public emacs-consult-mu
  (package
   (name "emacs-consult-mu")
   (version "20210426")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/armindarvish/consult-mu")
           (commit
            "90db1c6e3d0ec16126a347f6c15426c2a8ce0125")))
     (sha256
      (base32
       "0n7rxs6v3pcdvyb01l7l9msfdcfns94qj8gknkcsm7y75vgvjnmj"))))
   (build-system melpa-build-system)
   (arguments '(#:phases
                (modify-phases %standard-phases
                               (add-after 'unpack 'move-extras
                                          (lambda _ (copy-recursively "extras" "."))))))
   (home-page
    "https://github.com/armindarvish/consult-mu")
   (synopsis
    "Use consult to search mu4e dynamically or asynchronously")
   (description
    "Consult-mu provides a dynamically updated search interface to mu4e. It uses the awesome package consult by Daniel Mendler and mu/mu4e by Dirk-Jan C. Binnema, and optionally Embark by Omar Antolín Camarena to improve the search experience of mu4e.")
   (license license:gpl3+)))
