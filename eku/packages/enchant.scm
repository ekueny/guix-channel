(define-module (eku packages enchant)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages groff)
  #:use-module (gnu packages check)
  #:use-module (gnu packages enchant)
  #:use-module (gnu packages pkg-config))

(define-public unittest-cpp-with-version-number
  (package
   (inherit unittest-cpp)
   (name "unittest-cpp-with-version-number")
   (arguments
    `(#:tests? #f
      #:phases (modify-phases
                %standard-phases
                ;; copy github.com/unittest-cpp/unittest-cpp/pull/188 but has no effect
                ;; (add-before 'configure 'add-version-number
                ;;             (lambda _
                ;;               (substitute* "CMakeLists.txt"
                ;;                            (("^configure_file" all)
                ;;                             (string-append
                ;;                              "set(PACKAGE_VERSION ${CMAKE_PROJECT_VERSION})\n"
                ;;                              all)))))
                (add-after 'install 'add-version-number
                           (lambda* (#:key outputs #:allow-other-keys)
                                    (let* ((out (assoc-ref outputs "out"))
                                           (file (string-append out "/lib/pkgconfig/UnitTest++.pc")))
                                      (substitute* file
                                                   (("^Version: \n$")
                                                    (string-append
                                                     "Version: "
                                                     ,(package-version unittest-cpp)
                                                     "\n")))))))))))

(define-public enchant-2.8
  (package
   (inherit enchant)
   (version "2.8.1")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/AbiWord/enchant/releases"
                                "/download/v" version "/enchant-"
                                version ".tar.gz"))
            (sha256
             (base32
              "0kigwlb2dqbgf54i7z5bxiyyp94crqmrzi4xhi9nzccf1d3xwygz"))))
   (native-inputs (list groff pkg-config
                        unittest-cpp-with-version-number
                        `(,glib "bin")))))

enchant-2.8
