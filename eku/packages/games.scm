(define-module (eku packages games)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix build utils)
  #:use-module (guix build gnu-build-system)
  #:use-module (guix build-system copy)
  #:use-module (gnu packages java)
  #:use-module ((guix licenses) #:prefix license:)
  )

(define-public pilote
  (package
    (name "pilote")
    (version "2005")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://patrice.kueny.fr/logiciel/bin/pilot.jar"))
              (sha256
               (base32
                "1bqpl9j4jv98n8ysf0659d2khhv6idgg9rpx2pix5klah4xbr8wq"))))
    (inputs (list jbr21))
    (build-system copy-build-system)
    (arguments
     (list
      #:install-plan #~'(("pilot.jar" "share/java/"))
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'install 'make-wrapper-and-desktop
            (lambda* (#:key inputs outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (dir (string-append out "/share/java"))
                     (wrapper (string-append out "/bin/pilote"))
                     (desktop (string-append out "/share/applications/pilote.desktop"))
                     (java (search-input-file inputs "bin/java")))
                (mkdir-p (dirname wrapper))
                (mkdir-p (dirname desktop))
                (with-output-to-file wrapper
                  (lambda _
                    (format #t
                            (string-append
                             "cd ~a~%\n"
                             "~a -jar pilot.jar\n")
                            dir java)))
                (chmod wrapper #o755)
                (with-output-to-file desktop
                  (lambda _
                    (format #t
                            (string-append
                             "[Desktop Entry]\n"
                             "Name=Pilote\n"
                             "Exec=~a\n"
                             "Type=Application\n"
                             "Terminal=false\n"
                             "Categories=Game;\n")
                            wrapper)))))))))
    (home-page "https://patrice.kueny.fr/logiciel/")
    (synopsis "Jeu pilote")
    (description "Ce petit logiciel Java est un jeu graphique 2D où un ou deux enfants (à partir de 3 ans) pilotent des aéronefs (avion et hélicoptère). Le but du jeu est de décoller, d'éviter les crashs et de réussir à atterir. Avec un peu d'habileté, il est possible de faire un looping !")
    (license #f)))

(define-public alchemy
  (package
    (name "alchemy")
    (version "008")
    (source (origin
              (method url-fetch)
              (uri (string-append "http://al.chemy.org/files/Alchemy-" version ".tar.gz"))
              (sha256 (base32 "0449bvdccgx1jqnws1bckzs4nv2d230523qs0jx015gi81s1q7li"))))
    (inputs (list jbr21))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan '(("lib" "share/java/")
                        ("modules" "share/java/")
                        ("shapes" "share/java/")
                        ("Alchemy.jar" "share/java/"))
       #:phases
       (modify-phases %standard-phases
         (add-after 'install 'make-wrapper-and-desktop
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (dir (string-append out "/share/java"))
                    (wrapper (string-append out "/bin/alchemy"))
                    (desktop (string-append out "/share/applications/alchemy.desktop"))
                    (java (search-input-file inputs "bin/java")))
               (mkdir-p (dirname wrapper))
               (mkdir-p (dirname desktop))
               (with-output-to-file wrapper
                 (lambda _
                   (format #t
                           (string-append
                            "cd ~a~%\n"
                            "~a -jar Alchemy.jar $@\n")
                           dir java)))
               (chmod wrapper #o755)
               (with-output-to-file desktop
                 (lambda _
                   (format #t
                           (string-append
                            "[Desktop Entry]\n"
                            "Name=Alchemy\n"
                            "Exec=~a\n"
                            "Type=Application\n"
                            "Terminal=false\n"
                            "Categories=Art;\n")
                           wrapper)))))))))
    (home-page "http://al.chemy.org/")
    (synopsis "An open drawing project")
    (description "Alchemy is an open drawing project aimed at exploring how we can sketch, draw, and create on computers in new ways. Alchemy isn’t software for creating finished artwork, but rather a sketching environment that focuses on the absolute initial stage of the creation process. Experimental in nature, Alchemy lets you brainstorm visually to explore an expanded range of ideas and possibilities in a serendipitous way.")
    (license license:gpl3+)))
