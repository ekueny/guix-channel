(define-module (eku packages python)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (gnu packages time)
  #:use-module (gnu packages check)
  #:use-module (gnu packages video)
  #:use-module (gnu packages music)
  #:use-module (gnu packages python)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages jupyter)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-compression)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module ((guix licenses) #:prefix license:))

(define-public python-scienceplots
  (package
   (name "python-scienceplots")
   (version "2.0.1")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/garrettj403/SciencePlots/")
                  (commit "566be9d79b8da2e9acbdaf47dab7503a2036caf7")))
            (sha256
             (base32
              "1r5mkcayqcvs55wgjciwb1r2g2gh653l1bdjvwzdm82zrfgdwhyq"))))
   (build-system python-build-system)
   ;; Fix the missing files directly in styles folder
   ;; They are here when installing with pip but not with python setup.py build
   (arguments
    `(#:phases
      (modify-phases %standard-phases
                     (add-before 'build 'find-all-styles
                                 (lambda _
                                   (copy-recursively
                                    "scienceplots/styles" "build/lib/scienceplots/styles"))))))
   (propagated-inputs (list python-matplotlib))
   (home-page "https://github.com/garrettj403/SciencePlots/")
   (synopsis "Format Matplotlib for scientific plotting")
   (description "This repo has Matplotlib styles to format your figures for scientific papers, presentations and theses.")
   (license license:expat)))


(define-public python-xarray
  (package
   (name "python-xarray")
   (version "2023.02.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/pydata/xarray")
                  (commit "6c45d7fc472e200ca39300d25294fcb7a0bef04d")))
            (sha256
             (base32
              "0zpzrbm5x6zx10zwj3wyljqp5y79c2x8k1xr4bb8wscp3b4pz74c"))))
   (build-system python-build-system)
   (native-inputs
    (list python-setuptools-scm python-pytest python-matplotlib))
   (propagated-inputs
    (list python-numpy python-pandas))
   (arguments
    `(#:phases
      (modify-phases %standard-phases
                     (replace 'check
                              (lambda _
                                (invoke "pytest"))))))
   (home-page "https://github.com/pydata/xarray")
   (synopsis "N-D labeled arrays and datasets in Python")
   (description "Xarray (formerly xray) makes working with labelled
multi-dimensional arrays simple, efficient, and fun!
Xarray introduces labels in the form of dimensions, coordinates and attributes
on top of raw NumPy-like arrays, which allows for a more intuitive, more
concise, and less error-prone developer experience.  The package includes a
large and growing library of domain-agnostic functions for advanced analytics
and visualization with these data structures.")
   (license license:asl2.0)))

(define-public python-e13tools
  (package
   (name "python-e13tools")
   (version "0.9.6")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "e13tools" version))
            (sha256
             (base32
              "1xfhahlhwwy5gxd2s3qqy32m4ia7q29ccpgn8x54wr2sh9hanvhw"))))
   (build-system python-build-system)
   (arguments `(#:tests? #f))  ; TypeError: don't know how to make test
   (propagated-inputs (list python-matplotlib python-numpy))
   (home-page "https://e13tools.readthedocs.io")
   (synopsis "A collection of utility functions")
   (description "This package provides a collection of utility functions.")
   (license license:bsd-3)))

(define-public python-cmasher
  (package
   (name "python-cmasher")
   (version "1.6.3")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "cmasher" version))
            (sha256
             (base32
              "08wzgh772kml7p16v803rg8yz05fvnhham2s1bziaqddb1p20bi4"))))
   (build-system python-build-system)
   (arguments `(#:tests? #f))  ; TypeError: don't know how to make test
   (propagated-inputs (list python-colorspacious python-e13tools
                            python-matplotlib python-numpy))
   (home-page "https://cmasher.readthedocs.io")
   (synopsis
    "Scientific colormaps for making accessible, informative and 'cmashing' plots")
   (description
    "The CMasher package provides a collection of scientific colormaps and utility functions to be used by different Python packages and projects, mainly in combination with matplotlib. The colormaps in CMasher are all designed to be perceptually uniform sequential using the viscm package; most of them are color-vision deficiency friendly; and they cover a wide range of different color combinations to accommodate for most applications. It offers several alternatives to commonly used colormaps, like chroma and rainforest for jet; sunburst for hot; neutral for binary; and fusion and redshift for coolwarm. ")
   (license license:bsd-3)))

(define-public python-param
  (package
   (name "python-param")
   (version "1.12.3")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "param" version))
            (sha256
             (base32
              "0s7wrcbz934bqp5fgcx413sbklx82qmgmh28f1g2zz3q79y0s241"))))
   (build-system python-build-system)
   (native-inputs (list python-flake8 python-pytest python-pytest-cov))
   (home-page "http://param.holoviz.org/")
   (synopsis
    "Make your Python code clearer and more reliable by declaring Parameters.")
   (description
    "Make your Python code clearer and more reliable by declaring Parameters.")
   (license license:bsd-3)))

(define-public python-pyct
  (package
   (name "python-pyct")
   (version "0.5.0")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "pyct" version))
            (sha256
             (base32
              "1856dbrcpc0nxxhlfh3dqzz7xxn5sdi600q45hsprqyqrg2lm7yx"))))
   (build-system python-build-system)
   (arguments `(#:tests? #f))  ; TypeError: don't know how to make test
   (propagated-inputs (list python-param))
   (native-inputs (list python-flake8 python-pytest))
   (home-page "https://github.com/pyviz-dev/pyct")
   (synopsis
    "Python package common tasks for users (e.g. copy examples, fetch data, ...)")
   (description
    "Python package common tasks for users (e.g. copy examples, fetch data, ...)")
   (license #f)))

(define-public python-colorcet
  (package
   (name "python-colorcet")
   (version "3.0.1")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "colorcet" version))
            (sha256
             (base32
              "02s8l82z81bh9zxg66nvl3k7894z83c74dwm3z4zl4ix6lh5liai"))))
   (build-system python-build-system)
   (arguments `(#:tests? #f))  ; too many requirements
   (native-inputs (list python-pyct))
   (home-page "https://colorcet.holoviz.org/")
   (synopsis
    "Collection of perceptually accurate colormaps")
   (description
    "Colorcet is a collection of perceptually accurate 256-color colormaps for use with Python plotting programs like Bokeh, Matplotlib, HoloViews, and Datashader.")
   (license license:bsd-3)))

(define-public python-plotly
  (package
   (name "python-plotly")
   (version "5.13.0")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "plotly" version))
            (sha256
             (base32
              "16h3anljkx3pfxzqavjqf7y1px4i6x1w6wgwj0bvjnhx0bjam8w1"))))
   (build-system python-build-system)
   (arguments `(#:tests? #f))  ; deprecated command
   (propagated-inputs (list python-tenacity))
   (home-page "https://plotly.com/python/")
   (synopsis
    "An open-source, interactive data visualization library for Python")
   (description
    "An open-source, interactive data visualization library for Python")
   (license license:expat)))

(define-public python-magpylib
  (package
   (name "python-magpylib")
   (version "4.2.0")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "magpylib" version))
            (sha256
             (base32
              "0ifvsqnpcla6lidlyap5mqf4isgpc4y073bxrhbkk7q9j0xf1b2a"))))
   (build-system python-build-system)
   (arguments `(#:tests? #f))  ; missing test files
   (propagated-inputs (list
                       python-matplotlib
                       python-plotly
                       python-scipy
                       python-pytest))
   (native-inputs (list))
   (home-page "https://github.com/magpylib/magpylib")
   (synopsis "Free Python3 package to compute magnetic fields.")
   (description "Free Python3 package to compute magnetic fields.")
   (license #f)))

(define-public mastodon-data-viewer
  (package
   (name "mastodon-data-viewer")
   (version "2023-01-15")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/blackle/mastodon-data-viewer.py")
                  (commit "74da9b4c308f52391d4ed244862e904a2ffb4285")))
            (sha256
             (base32
              "1i12lxzy0qphgmzsabvvy2jyk9375ayzrdisrfxqa79b0adgidny"))))
   (build-system copy-build-system)
   (arguments
    '(#:install-plan '(("mastodon-data-viewer.py" "bin/mastodon-data-viewer"))))
   (propagated-inputs (list
                       python-minimal
                       python-bigjson
                       python-tqdm
                       python-dateutil))
   (home-page "https://github.com/blackle/mastodon-data-viewer.py")
   (synopsis "A viewer for mastodon data written in python.")
   (description "A viewer for mastodon data written in python. It creates a local server that you can use to browse the data. Designed for large (>40,000) toot archives. Supports full text search and the following post content: content warnings, image attachments, video attachments, audio attachments, alt text, polls (oneOf or anyOf).")
   (license license:cc0)))

(define-public python-bigjson
  (package
   (name "python-bigjson")
   (version "1.0.9")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "bigjson" version))
            (sha256
             (base32
              "1wvhs40x89mcnyy0dga85wpjl9cq310fspmk6ssfl0hcgq0669hc"))))
   (build-system python-build-system)
   (home-page "https://henu.fi/bigjson")
   (synopsis "Python library that reads JSON files of any size.")
   (description "I was using Python to parse the wikidata dumps that are tens of gigabytes of JSON. I though it would be nice if I could open the JSON even though it cannot fit to the memory. That is not possible with the built in JSON module, so I made this library. It reads JSON files of any size.")
   (license license:expat)))

(define-public python-mastodon-py
  (package
   (name "python-mastodon-py")
   (version "1.8.1")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "Mastodon.py" version))
     (sha256
      (base32
       "0zk8sh2420w947yq90k3bzx4hq26bkdsz3jb7vbsvmmdmfacnr2a"))))
   (build-system python-build-system)
   (arguments `(#:tests? #f)) ; some are failing
   (propagated-inputs
    (list
     python-blurhash
     python-dateutil
     python-decorator
     python-magic
     python-pytz
     python-requests
     ;; python-six
     ))
   (native-inputs
    (list
     python-blurhash
     ;; python-cryptography
     ;; python-http-ece
     python-pytest
     python-pytest-cov
     python-pytest-mock
     python-pytest-runner
     python-pytest-vcr
     python-requests-mock
     ;; python-vcrpy
     ))
   (home-page "https://github.com/halcy/Mastodon.py")
   (synopsis "Python wrapper for the Mastodon API")
   (description
    "This package provides a python wrapper for the Mastodon API.")
   (license license:expat)))

(define-public mastodon-archive
  (package
   (name "mastodon-archive")
   (version "1.4.4")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/kensanata/mastodon-archive")
                  (commit "8c5d9c0618df5cb9d793a785d04203bedfc4d49c")))
            (sha256
             (base32
              "0iqjla6qhh3lcaq7i7ccjm0s18nxh0j9d4vydcl5h6jfih2wlakg"))))
   (build-system python-build-system)
   (propagated-inputs (list
                       python-html2text
                       python-mastodon-py
                       python-progress))
   (home-page "https://src.alexschroeder.ch/mastodon-archive.git/")
   (synopsis "Utility for backing up your Mastodon content")
   (description "Mastodon Archive is a command line tool to create an archive of your toots, favourites and mentions such that you can search them, turn them into text files, static HTML, or expire them.")
   (license license:gpl3)))

;; personal version including packages required for postprocessing
(define-public gallery-dl
  (package
   (name "gallery-dl-eku")
   (version "1.24.5-20230223")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/mikf/gallery-dl")
                  (commit "6ed4309aba44bc52719664cc832ecfe89c3864ec")))
            (sha256
             (base32
              "1hiyqaq21zxpylizcqi4367sf474pxlpjkxjlky4wl5gk7qidvhx"))))
   (build-system python-build-system)
   (arguments `(#:tests? #f))  ; takes too long
   (inputs (list python-requests ffmpeg))
   (propagated-inputs (list python-wrapper python-beautifulsoup4))
   (home-page "https://github.com/mikf/gallery-dl")
   (synopsis "Command-line program to download images from several sites")
   (description "Gallery-dl is a command-line program that downloads image
galleries and collections from several image hosting sites.  While this package
can use youtube-dl or yt-dlp packages to download videos, the focus is more on
images and image hosting sites.")
   (license license:gpl2)))

(define-public python-pdfminer.six
  (package
   (name "python-pdfminer.six")
   (version "20231228")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "pdfminer.six" version))
     (sha256
      (base32 "1i08rdxx6f58ckzscrg37gknic49vy9h75fb61cx9957s4xdl130"))))
   (build-system pyproject-build-system)
   (arguments `(#:tests? #f))
   (propagated-inputs (list python-charset-normalizer python-cryptography
                            python-importlib-metadata
                            python-typing-extensions))
   (native-inputs (list python-black python-mypy python-nox python-pytest))
   (home-page "https://github.com/pdfminer/pdfminer.six")
   (synopsis "PDF parser and analyzer")
   (description "PDF parser and analyzer")
   (license #f)))

(define-public python-pdfannots
  (package
   (name "python-pdfannots")
   (version "0.4")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "pdfannots" version))
     (sha256
      (base32 "1lr7axvc9va2v7s8adygbik5mg0l5ghq13nhj62c3iw37971lzcn"))))
   (build-system pyproject-build-system)
   (arguments '(#:tests? #f ; doesn't find pdfminer
                #:phases (modify-phases %standard-phases
                                        (delete 'sanity-check))))
   (propagated-inputs (list python-pdfminer.six))
   (home-page "https://github.com/0xabu/pdfannots")
   (synopsis "Tool to extract and pretty-print PDF annotations for reviewing")
   (description
    "Tool to extract and pretty-print PDF annotations for reviewing")
   (license license:expat)))

(define-public python-opensubtitlescom
  (package
   (name "python-opensubtitlescom")
   (version "0.1.4")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "opensubtitlescom" version))
     (sha256
      (base32 "1dmzxzvmqmkzgplag8fwyjmsk3p1jphzjqfx4b87nj0zhpw14f4g"))))
   (build-system pyproject-build-system)
   (propagated-inputs (list python-prettytable python-requests))
   (arguments '(#:tests? #f
                #:phases (modify-phases %standard-phases
                                        (delete 'sanity-check))))
   (home-page "https://github.com/dusking/opensubtitles-com")
   (synopsis "OpenSubtitles.com new REST API")
   (description "@code{OpenSubtitles.com} new REST API.")
   (license #f)))

(define-public python-yt-dlp
  (package
   (name "python-yt-dlp")
   (version "2024.5.27")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "yt_dlp" version))
     (sha256
      (base32 "1gl8aafxh04bh534sqwdc7y8v8rcyxaycp18qb8x630d4kgc0rim"))))
   (build-system pyproject-build-system)
   (propagated-inputs (list python-brotli
                            python-brotlicffi
                            python-certifi
                            python-mutagen
                            python-pycryptodomex
                            python-requests
                            python-urllib3
                            python-websockets))
   (native-inputs (list python-hatchling))
   (arguments
    '(#:tests? #f
      #:phases (modify-phases %standard-phases
                              (delete 'sanity-check))))
   (home-page "https://github.com/yt-dlp/yt-dlp")
   (synopsis "A feature-rich command-line audio/video downloader")
   (description
    "This package provides a feature-rich command-line audio/video downloader.")
   (license #f)))
