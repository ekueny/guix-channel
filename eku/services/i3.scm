(define-module (eku services i3)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages wm)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu home services)
  #:export (home-i3-service-type
            home-i3-configuration
            mode-configuration))

(define (serialize-list-of-values field-name value)
  #~(string-append
     #$@(map
         (lambda (x) (string-append (symbol->string field-name) " " x "\n"))
         value) "\n"))

(define (serialize-list-of-strings field-name value)
  #~(string-append
     #$@(map (lambda (x) (string-append x "\n")) value) "\n"))

(define (serialize-bar field-name value)
  #~(string-append "bar {\n"
                   #$@(map (lambda (x) (string-append x "\n")) value) "}\n"))

(define (serialize-list-of-values-with-criteria field-name value)
  #~(string-append
     #$@(map
         (lambda (pair) (string-append (symbol->string field-name)
                                       " [" (car pair) "] " (cadr pair) "\n"))
         value) "\n"))

(define (list-of-mode-configurations? lst)
  #t) ; don't know why it complains

(define (serialize-list-of-mode-configurations field-name value)
  #~(string-append #$@(map
                       (lambda (mode)
                         (serialize-configuration
                          mode mode-configuration-fields))
                       value) "}\n"))

(define (serialize-mode-name field-name value)
  #~(string-append "mode \"" #$value "\" {\n"))

(define-configuration mode-configuration
  (name
   (string)
   "The name of the mode."
   serialize-mode-name)

  (bindsym
   (list '())
   "List of keybinding in the form @code{'((key arguments))}."
   serialize-list-of-values))

(define-configuration home-i3-configuration
  (package
   (package i3-gaps)
   "The i3 package to use.")

  (set
   (list '())
   "List strings for variable assignments."
   serialize-list-of-values)

  (extra
   (list '())
   "List of unformatted strings."
   serialize-list-of-strings)

  (bindsym
   (list '())
   "List of strings for keybinding."
   serialize-list-of-values)

  (assign
   (list '())
   "List of assignments as pair of strings."
   serialize-list-of-values-with-criteria)

  (for_window
   (list '())
   "List of windows assigments as pair of strings."
   serialize-list-of-values-with-criteria)

  (modes
   (list-of-mode-configurations '())
   "A list of @code{mode-configuation}.")

  (bar
   (list '())
   "List of settings strings for the status bar."
   serialize-bar))

(define (i3-config-service i3-config)
  `(("i3/config"
     ,(mixed-text-file
       "i3-config"
       #~(string-append "# i3 config file (v4)\n\n")
       (serialize-configuration
        i3-config
        home-i3-configuration-fields)))))

(define home-i3-service-type
  (service-type
   (name 'home-i3)
   (extensions
    (list
     (service-extension home-xdg-configuration-files-service-type
                        i3-config-service)))
   (default-value (home-i3-configuration))
   (description "Install and configure i3.")))
