(define-module (eku services shepherd)
  #:use-module (srfi srfi-1) ; find, remove
  #:use-module (gnu services)
  #:use-module (gnu services shepherd))

(define-public (make-service-type-without-autostart original-service-type)
  (define (original-shepherd-service original-conf)
    (let ((original-shepherd-service
           (service-extension-compute
            (find (lambda (ext)
                    (eq? (service-extension-target ext) shepherd-root-service-type))
                  (service-type-extensions original-service-type)))))
      (list
       (shepherd-service
        (inherit (car (original-shepherd-service original-conf)))
        (auto-start? #f)))))
  (service-type
   (inherit original-service-type)
   (extensions
    (cons
     (service-extension shepherd-root-service-type original-shepherd-service)
     (remove (lambda (ext)
               (eq? (service-extension-target ext) shepherd-root-service-type))
             (service-type-extensions original-service-type))))))
